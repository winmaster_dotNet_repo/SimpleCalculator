﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KatarzynaBialasCalculator
{
    public partial class Calculator : Form
    {
        //zapamietuje wynik w globalnej zmiennej- możliwosc wykorzystania
        double globalResult;

        public Calculator()
        {
            InitializeComponent();
            globalResult = 0;
        }

        #region Podstawowe przyciski liczb

        private void buttonOne_Click(object sender, EventArgs e)
        {
            textBoxResults.Text += "1";
        }

        private void buttonTwo_Click(object sender, EventArgs e)
        {
            textBoxResults.Text += "2";
        }

        private void buttonThree_Click(object sender, EventArgs e)
        {
            textBoxResults.Text += "3";
        }

        private void buttonFour_Click(object sender, EventArgs e)
        {
            textBoxResults.Text += "4";
        }

        private void buttonFive_Click(object sender, EventArgs e)
        {
            textBoxResults.Text += "5";
        }

        private void buttonSix_Click(object sender, EventArgs e)
        {
            textBoxResults.Text += "6";
        }

        private void buttonSeven_Click(object sender, EventArgs e)
        {
            textBoxResults.Text += "7";
        }

        private void buttonEight_Click(object sender, EventArgs e)
        {
            textBoxResults.Text += "8";
        }

        private void buttonNine_Click(object sender, EventArgs e)
        {
            textBoxResults.Text += "9";
        }

        private void buttonZero_Click(object sender, EventArgs e)
        {
            textBoxResults.Text += "0";
        }

        #endregion

        #region Przyciski funkcyjne

        private void buttonComa_Click(object sender, EventArgs e)
        {
            textBoxResults.Text += ",";
        }

        private void buttonPlus_Click(object sender, EventArgs e)
        {
            textBoxResults.Text += "+";
        }

        private void buttonMinus_Click(object sender, EventArgs e)
        {
            textBoxResults.Text += "-";
        }

        private void buttonMultiply_Click(object sender, EventArgs e)
        {
            textBoxResults.Text += "*";
        }

        private void buttonDivide_Click(object sender, EventArgs e)
        {
            textBoxResults.Text += "/";
        }
        #endregion


        #region Metody i dzialania operujace funkcjami kalkulatora


        private double add(double a, double b)
        {
            return a + b;
        }

        private double minus(double a, double b)
        {
            return a - b;
        }

        private double multiply(double a, double b)
        {
            return a * b;
        }

        private double divide(double a, double b)
        {
            if(b==0.0)
            {
                MessageBox.Show("Nie umiem dzielić przez 0 :(");
                Close();
            }

            return a / b;
        }


       
        private void buttonResult_Click(object sender, EventArgs e)
        {
                                 
            String result = "";
                result= textBoxResults.Text;

            int size = result.Length;

            if (result=="" || size==0)
            {
                MessageBox.Show("Zły ciąg wartości :(");
                Close();
            }


            textBoxResults.Text += "=";

 
            if(result[size-1]=='*' || result[size-1] == '+' || result[size-1] == '/' || result[size-1] == '-'|| result[size-1] == ',')
            {
                MessageBox.Show("Zły ciąg wartości :( - ostatni znak nie może być operrandem");
                 Close();
            }

            String[] numbers = result.Split(new char[] {'+', '-', '*', '/' });
            String operations="";

            if(numbers.Length<2)
            {
                MessageBox.Show("Zły ciąg wartości :( za mało argumentów");
                Close();
            }

            //asercja: nie moze byc conajmniej dwoch znakow dzialan obok siebie nieprzedzielonych liczbą
            int operationCounter = 0;

            for(int i=0; i< size; i++)
            {
                if(result[i] == '*' || result[i] == '+' || result[i] == '/' || result[i] == '-')
                {
                    operations += result[i];
                    operationCounter++;
                }
                else
                {
                    operationCounter = 0;
                }

                if(operationCounter>1)
                {
                    MessageBox.Show("Nie wiem co mam wykonać");
                    Close();
                }
            }

            double partialSum= Double.Parse(numbers[0]);
            double partialResult=0;
            double number1=0;

            int j = 0; //index numbers

            //rozmiar tablicy operacji jest o 1 mniejszy od rozmiaru tablicy liczb

            while(j<numbers.Length-1)
            {
                char a = operations[j];

                number1 = Double.Parse(numbers[j + 1]);

                switch(a)
                {
                    case '*':
                        {
                            partialResult = multiply(number1, partialSum);
                            break;
                        }
                    case '+':
                        {
                            partialResult= add(number1, partialSum);
                            break;
                        }
                    case '-':
                        {
                            partialResult = minus(partialSum, number1);
                            break;
                        }
                    case '/':
                        {
                            partialResult=divide(partialSum, number1);
                            break;
                        }
                }

                partialSum = partialResult;
                j++;
            }

            globalResult = partialSum;

            textBoxResults.Text=globalResult.ToString();


        }

        #endregion

        #region Przyciski pomocniczne

        private void buttonClear_Click(object sender, EventArgs e)
        {
            textBoxResults.Text = "";
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            String tmp = textBoxResults.Text;
            int size = tmp.Length;

            String newText = "";

            for(int i=0; i<size-1; i++)
            {
                newText += tmp[i];
            }

            textBoxResults.Text = newText;
        }
        #endregion
    }
}
